from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read


# Nome del file
file_name = '../sounds/nota.wav'

# Soglia
threshold = int(input('Valore di soglia per individuazione dei picchi (dB): '))

# funzione per convertire decibel a valori lineari
def dbtoa(n):
    return np.power(10, n / 20)

# funzione per convertire valori lineari a decibel
def atodb(n):
    # evito lo zero
    aval = np.maximum(n, np.finfo(float).eps)
    return 20 * np.log10(aval)

# funzione per il calcolo del valore rms
def rms(array):
    return np.sqrt(np.mean(np.square(array)))

# funzione per individyare i picch
# ===> array da analizzare, soglia minima, tempo salita e discesa (in campioni)
def find_peaks(array, threshold, wait):
    # array dove inserire i picchi
    peaks = []
    
    # variabile temporanea per salvare l'ultimo indice di valore maggiore
    idx = 0
    
    # flag utilizzare per salita e decadimento
    flag = 0
    
    # flag per tenere attiva l'indicizzazione finché il valore è in crescita
    flag2 = 0
    
    # rispetto alla soglia, effettuo il calcolo del valoro rms
    meanval = []
    for x in array:
        if (x > threshold):
            meanval.append(x)
    meanval = rms(meanval)
    
    for i in range(1, len(array)):
        if array[i] > meanval:
            
            # finché il valore cresce, aggiorno l'indice di picco
            if array[i] > array[i-1] and (flag <= 0 or flag2 == 1):
                flag = 1
                flag2 = 1
                idx = i
            
            # se la direzione dei valori di ampiezza cambia, e non si è in wait
            elif array[i] < array[i-1] and flag == (1 - 1/wait):
                
                # e se il valore non è solo 'momentaneamente' oscillatorio
                if array[i-1] > array[min(i+1, len(array))]:
                    flag2 = 0
                    meanval = []
                    
                    # effettuo il nuovo valore di confronto per l'individuazione dei picchi
                    for x in range(idx+int(wait), len(array)):
                        if (array[x] > threshold):
                            meanval.append(array[x])
                    meanval = rms(meanval)
                    
                    # salvo l'indice i picco
                    peaks.append(idx)
                
        # decadimento temporale per la funzione di wait
        flag -= 1/wait
    return np.array(peaks)    
    
# leggo il campione fornito dall'argomento
[fs, segnale] = read(file_name)

# normalizzazione del segnale
segnale = segnale / np.max(abs(segnale))

# effettuo stft, con finestratura 8192, zero padding e finestratura blackman
winSize = 8192
f, t, stft = signal.stft(segnale, fs, window='blackman', nperseg=winSize, noverlap=winSize//2, nfft=winSize, return_onesided=True, padded=True)

# calcolo le ampiezze e prendo solo un picco (1) per bin
magnitude = np.max(np.abs(stft), axis=1)

# indicizzo le frequenze
indexes = np.arange(len(magnitude))
frequencies = [(idx * (fs/winSize)) for idx in range(len(magnitude))]

# conto quante armoniche sono presenti, rispetto alla soglia, con decadimento pari 60Hz in step di finestratura
harm_idx = find_peaks(magnitude, dbtoa(threshold), (60/(fs/winSize)))

# individuazione della fondamentale
f0 = harm_idx[0] * (fs/winSize)
print('Frequenza fondamentale', f0)

# array per salvare la differenza tra i valori degli indici
scarto = []

# creo un array contenente le armoniche ideali con le stesse ampiezze
fixed_harmonics_bffr = np.zeros(len(magnitude))
for idx, x in enumerate(harm_idx):
    harmonic = f0 * (idx+1)
    
    # adatto l'indice a partire dalla frequenza rispetto allo step dato dalla finestra di analisi winSize
    new_idx = int(harmonic/(fs/winSize))
    fixed_harmonics_bffr[new_idx] = magnitude[x]
    
    # differenza di indice - armonicità
    scarto.append(abs(x-new_idx))

# scarto quadratico medio in Hz
std = np.std(scarto)*(fs/winSize)
print('Indice di Inarmonicità (Hz)', '{}Hz'.format(std))

# plot dell'analisi dello spettro
plt.plot(frequencies, magnitude)
plt.plot(frequencies, fixed_harmonics_bffr, linestyle='dashed')
plt.xlabel('Frequenza')
plt.ylabel('Ampiezza')
plt.title('Picchi spettrali nella STFT')
plt.show()
