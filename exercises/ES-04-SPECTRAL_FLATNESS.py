import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read, write
from scipy.signal import stft
import math

def sts(sig, frameSize):
    #   calcolo il numero di frame arrotondato all'intero più grande
    Nframe = math.ceil(len(sig) / frameSize)

    #   inizializzo la matrice di Nframe righe e frameSize colonne
    STS = np.zeros((Nframe, frameSize))

    #   copio il segnale in un nuovo vettore
    sigFramed = np.zeros(Nframe * frameSize)
    sigFramed[:len(sig)] = sig

    #   creo finestratura blackman
    winb = np.blackman(frameSize)

    for i in range(Nframe):
        STS[i,:] = sigFramed[(i*frameSize):(i+1)*frameSize] * winb

    return STS

def spectral_flatness(v): 
    AMP = np.abs(np.fft.rfft(v)) 
    L = len(AMP)
    GM = (np.prod(AMP+1)-1)**(1.0/L)
    AM = np.sum(AMP)/L
    return 10*np.log10(GM/AM)

#   lettura file audio
[fs, segnale] = read('../sounds/speech-male.wav')

# normalizzazione del segnale
segnale = segnale / np.max(abs(segnale))

STS = sts(segnale, 1024)
SF = [spectral_flatness(f) for f in STS]

plt.plot(SF, 'b', label='flatness')
plt.ylabel('Flatness [dB]')
plt.xlabel('Time [sec]')
plt.legend()
plt.show()
