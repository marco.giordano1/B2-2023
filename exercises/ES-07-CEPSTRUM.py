from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read

def fundamental_frequency(bffr, sample_rate):

    # trovo il picco nel cepstrum
    min_index = int(sample_rate / 400) 
    max_index = int(sample_rate / 70) 
    peak_index = np.argmax(bffr[min_index:max_index]) + min_index
    
    # converto da picco a frequenza
    fundamental_freq = sample_rate / peak_index

    return fundamental_freq

[fs, segnale1] = read('../sounds/vocali_a.wav')
[fs, segnale2] = read('../sounds/vocali_i.wav')

winSize = 8192
f, t, stft = signal.stft(segnale1, fs, window='blackman', nperseg=winSize, noverlap=winSize//2, nfft=winSize, return_onesided=True, padded=True)
log_X = np.log(np.abs(np.transpose(stft)))
FF1 = [fundamental_frequency(f, fs) for f in log_X]
f, t, stft = signal.stft(segnale2, fs, window='blackman', nperseg=winSize, noverlap=winSize//2, nfft=winSize, return_onesided=True, padded=True)
log_X = np.log(np.abs(np.transpose(stft)))
FF2 = [fundamental_frequency(f, fs) for f in log_X]


plt.subplot(211)
plt.title('Fundamental Frequency')
plt.plot(FF1)
plt.subplot(212)
plt.plot(FF2)
plt.show()