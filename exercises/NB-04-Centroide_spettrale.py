#
# Centroide spettrale
#
import pdb
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read, write
from scipy.fft import fft
from scipy.signal import stft
import math

##### MAIN

# leggo file audio

[fs, segnale] = read("../sounds/speech-male.wav")

# normalizziamo il segnale
max_segnale = np.max(np.abs(segnale))
segnale = segnale / max_segnale
dur = len(segnale)/fs

#
# da qui inizia l'esercizio 04 vero e proprio
#
inner_frame = 1001
f, t, STFT = stft(segnale, fs=fs, nperseg=inner_frame, noverlap=inner_frame//4, nfft=2048)
binsize=fs/inner_frame
abs_STFT = np.abs(STFT)

plt.figure(2, figsize=(16, 8))
plt.pcolormesh(t, f, abs_STFT, vmin=0, vmax=np.max(abs_STFT)/5, shading='gouraud')
plt.axis([0, len(segnale)/fs, 0, 4000])
plt.title('STFT Magnitude')
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
plt.show()

#
# calcolo della centroide
#
def spectral_centroid(frame):
    abs_frame = np.abs(frame)
    num = np.sum([n[0]*n[1] for n in enumerate(abs_frame)])
    den = np.sum(abs_frame)
    return num/den

#
# la STFT ritorna i bins nelle colonne e i frames nelle righe, quindi devo
# ruotare la matrice di 90 gradi.
#
rot_STFT = np.transpose(STFT)
centroid = np.zeros(len(t))
for n, frame in enumerate(rot_STFT):
    centroid[n] = spectral_centroid(frame)

centroid *= binsize
plt.figure(1, figsize=(16,8))
plt.plot(t, centroid, 'b', label='centroid')
plt.axis([0, t[-1], -1, np.max(centroid)*1.1])
plt.ylabel('Frequency [bin index]')
plt.xlabel('Time [sec]')
plt.legend()
plt.show()
