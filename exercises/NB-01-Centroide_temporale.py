#
# cannibalizzato dallo script ../scripts/6-short time signal analysis.py
#
import pdb
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read, write
from scipy.fft import fft
import math

# CREAZIONE DELLA MATRICE DELLE FRAME TEMPORALI

def sts (sig, frameSize):
    # calcolo il numero di frame arrotondato all'intero più grande
    Nframe = math.ceil(len(sig)/frameSize)

    STS = np.zeros((Nframe,frameSize))

    # copio il segnale in un nuovo vettore
    sigFramed = np.zeros(Nframe*frameSize)

    sigFramed[:len(sig)] = sig

    for i in range(Nframe):
        STS[i,:] = sigFramed[(i*frameSize):((i+1)*frameSize)]

    return [STS, Nframe]

# RMS

def rms(v):
    v = np.square(v)
    v = np.mean(v)
    v = np.sqrt(v)
    return v

##### MAIN

# leggo file audio

[fs, segnale] = read("../sounds/speech-male.wav")

# normalizziamo il segnale
segnale = segnale / np.max(abs(segnale))

#
# da qui inizia l'esercizio 01 vero e proprio
#
frame_size = 600
(STS, nframes) = sts(segnale,frame_size)

#
# calcolo della centroide
#
centroid = np.zeros((nframes, 1))
idx = 0
for frame in STS:
    num = np.sum([n[0]*n[1] for n in enumerate(frame)])
    div = np.sum(frame)
    centroid[idx] = np.abs(num) / np.abs(div)
    idx += 1


plt.figure(1, figsize=(16,8))
plt.plot(centroid, 'b', label='centroid')
plt.axis([0, nframes, -10, 5000])
plt.legend()
plt.show()
