import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read, write
import math

#   CREAZIONE DELLA MATRICE DELLE FRAME TEMPORALI
def sts(sig, frameSize):
    #   calcolo il numero di frame arrotondato all'intero più grande
    Nframe = math.ceil(len(sig) / frameSize)

    #   inizializzo la matrice di Nframe righe e frameSize colonne
    STS = np.zeros((Nframe, frameSize))

    #   copio il segnale in un nuovo vettore
    sigFramed = np.zeros(Nframe * frameSize)
    sigFramed[:len(sig)] = sig

    for i in range(Nframe):
        STS[i,:] = sigFramed[(i*frameSize):(i+1)*frameSize]

    return STS

#   RMS - root mean square
def rms(v):
    v = np.square(v)
    v = np.mean(v)
    v = np.sqrt(v)
    return v

def energy(v):
    v = np.sum(np.square(v))
    return v

#   #  MAIN
#   lettura file audio
[fs, segnale] = read('sounds/speech-male.wav')

# normalizziamo il segnale
segnale = segnale / np.max(abs(segnale))

STS = sts(segnale, 1000)
RMS = [rms(f) for f in STS]
ENE = [energy(f) for f in STS]

plt.subplot(211)
plt.plot(RMS, 'r')

plt.subplot(212)
plt.plot(ENE, 'b')

plt.show()