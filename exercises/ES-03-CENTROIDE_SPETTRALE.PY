import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read, write
from scipy.signal import stft
import math

def sts(sig, frameSize):
    #   calcolo il numero di frame arrotondato all'intero più grande
    Nframe = math.ceil(len(sig) / frameSize)

    #   inizializzo la matrice di Nframe righe e frameSize colonne
    STS = np.zeros((Nframe, frameSize))

    #   copio il segnale in un nuovo vettore
    sigFramed = np.zeros(Nframe * frameSize)
    sigFramed[:len(sig)] = sig

    #   creo finestratura blackman
    winb = np.blackman(frameSize)

    for i in range(Nframe):
        STS[i,:] = sigFramed[(i*frameSize):(i+1)*frameSize] * winb

    return STS

def spectral_centroid(v, fs):
    AMP = np.abs(np.fft.rfft(v))
    L = len(v)
    FREQ = np.abs(np.fft.fftfreq(L, 1.0/fs)[:L//2+1])
    return np.sum(AMP*FREQ) / np.sum(AMP)

#   lettura file audio
[fs, segnale] = read('../sounds/speech-male.wav')

# normalizzazione del segnale
segnale = segnale / np.max(abs(segnale))

STS = sts(segnale, 1024)
CS = [spectral_centroid(f, fs) for f in STS]

plt.plot(CS, 'b', label='centroid')
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
plt.legend()
plt.show()
