import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import read, write
import math

#   CREAZIONE DELLA MATRICE DELLE FRAME TEMPORALI
def sts(sig, frameSize):
    #   calcolo il numero di frame arrotondato all'intero più grande
    Nframe = math.ceil(len(sig) / frameSize)

    #   inizializzo la matrice di Nframe righe e frameSize colonne
    STS = np.zeros((Nframe, frameSize))

    #   copio il segnale in un nuovo vettore
    sigFramed = np.zeros(Nframe * frameSize)
    sigFramed[:len(sig)] = sig

    for i in range(Nframe):
        STS[i,:] = sigFramed[(i*frameSize):(i+1)*frameSize]

    return STS


#   TEMPORAL CENTROID
def tc(v):
    tmp = np.sum(v)
    for i in range(len(v)):
        v[i] = i*v[i]
    v = np.sum(v)
    v = v/tmp
    return v

#   #  MAIN
#   lettura file audio
[fs, segnale] = read('sounds/speech-male.wav')

# normalizziamo il segnale
segnale = segnale / np.max(abs(segnale))

STS = sts(segnale, 1000)
CEN = [tc(f) for f in STS]

plt.subplot(211)
plt.plot(CEN, 'r')

plt.subplot(212)
plt.plot(segnale, 'b')

plt.show()