from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import write, read
from s0_functions import *

# leggo un file audio in formato wav
[fs, segnale]=read("../sounds/speech-female.wav")

print("SR:", fs, "\ndimensione del file audio:", np.size(segnale))

# normalizzo ad 1
data = segnale * 1.0 / np.max(abs(segnale))

winSize = 1024
threshold = -22

f, t, stft = signal.stft(data, fs, window='blackman', nperseg=winSize, noverlap=winSize//2, nfft=winSize, return_onesided=True, padded=True)

print(np.shape(stft))

SF = s_flatness(np.transpose(stft)) # estraggo la spectral flatness

sf_indexes_vow = np.where(SF < threshold)[0]
sf_indexes_con = np.where(SF >= threshold)[0]

segments_vow = np.take(stft, sf_indexes_vow, axis = 1)
segments_con = np.take(stft, sf_indexes_con, axis = 1)
t2, segmented_vow = signal.istft(segments_vow, fs, window='blackman', nperseg=winSize,noverlap=winSize//2, nfft=winSize, input_onesided='True')
t3, segmented_con = signal.istft(segments_con, fs, window='blackman', nperseg=winSize,noverlap=winSize//2, nfft=winSize, input_onesided='True')

write("./script-outputs/vocali.wav", fs, np.real(segmented_vow))
write("./script-outputs/consonanti.wav", fs, np.real(segmented_con))

plt.figure(1)
plt.pcolormesh(np.abs(stft), vmin=0, vmax=np.max(np.abs(stft))/4)
plt.title('STFT Magnitude')
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')

plt.figure(2)
plt.subplot(311)
plt.ylabel("segnale")
plt.plot(segnale, 'b')
plt.grid()
plt.subplot(312)
plt.ylabel("sp. flatness")
plt.plot(SF, 'm')
plt.grid()
plt.subplot(313)
plt.ylabel("vocali estratte")
plt.plot(segmented_vow, 'r')
plt.grid()

plt.show()
