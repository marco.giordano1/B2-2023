from s0_functions import *

N = 512
k0 = 6

nv = np.arange(-N/2, N/2)
kv = np.arange(-N/2, N/2)

x = np.cos(2*np.pi*k0*nv/N)

AX = autocorr(x)

plt.figure(1)
plt.subplot(211)
plt.plot(x)
plt.subplot(212)
plt.plot(AX)
plt.show()
