import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft, fftshift

N = 64
k0 = 2

n = np.arange(-N/2,N/2)

x = np.sin(2*np.pi * k0 * n / N)
#x = np.exp(1j*2*np.pi*k0*n/N)

X = fftshift(fft(x))

e_n = np.sum(np.square(x))
e_k = np.sum(np.square(np.abs(X))) / np.size(X)

print("energia nel dominio del tempo: ", e_n, "\nenergia nel dominio della frequenza: ", e_k)

plt.plot(n, np.real(x), 'b', lw=2, label="real")
plt.plot(n, np.imag(x),'g', lw=2, label="imaginary")
plt.xlabel('indice td')
plt.ylabel('ampiezza')
plt.axis([-N/2,N/2,-1,1])
plt.legend()

plt.figure(2)
plt.plot(np.arange(0,N), np.abs(X), 'ro', lw=2, label="modulo")

plt.show()