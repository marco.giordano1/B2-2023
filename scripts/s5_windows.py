import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy.fftpack import fft, fftshift

N = 51
M = 256

window1 = signal.boxcar(N)
window2 = signal.hamming(N)
window3 = signal.blackman(N)

W1 = fft(window1, M) / (M/2)
W1 = np.abs(fftshift(W1))
W1 = 20*np.log10(W1)
W2 = fft(window2, M) / (M/2)
W2 = np.abs(fftshift(W2))
W2 = 20*np.log10(W2)
W3 = fft(window3, M) / (M/2)
W3 = np.abs(fftshift(W3))
W3 = 20*np.log10(W3)

plt.figure(1)
plt.plot(window1)
plt.plot(window2)
plt.plot(window3)
plt.figure(2)
plt.plot(W1)
plt.plot(W2)
plt.plot(W3)
plt.show()
