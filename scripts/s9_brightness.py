from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
from scipy.io.wavfile import write, read
from s0_functions import *

# leggo un file audio in formato wav
[fs, segnale]=read("../sounds/speech-male.wav")

print("SR:", fs, "\ndimensione del file audio:", np.size(segnale))

# normalizzo ad 1
data = segnale * 1.0 / np.max(abs(segnale))
winSize = 2048
f, t, stft = signal.stft(data, fs, window='blackman', nperseg=winSize, noverlap=winSize//2, nfft=winSize, return_onesided=True, padded=True)

print(np.shape(stft))

SC = s_centroid(np.transpose(stft))/winSize*fs
SF = s_flatness(np.transpose(stft))

plt.figure(1)
plt.subplot(311)
plt.ylabel("segnale")
plt.plot(segnale, 'b')
plt.grid()
plt.subplot(312)
plt.ylabel("brightness")
plt.plot(SC, 'g')
plt.grid()
plt.subplot(313)
plt.ylabel("s. flatness")
plt.plot(SF, 'r')
plt.grid()

plt.figure(2)
plt.pcolormesh(np.abs(stft), vmin=0, vmax=np.max(np.abs(stft))/5)
plt.title('STFT Magnitude')
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')

plt.show()