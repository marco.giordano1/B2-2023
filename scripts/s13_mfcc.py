from s0_functions import *
from scipy.io.wavfile import read
from scipy import signal
from scipy.fftpack import dct

# leggo un file audio in formato wav
[fs, segnale]=read("../sounds/speech-female.wav")

print("SR:", fs, "\ndimensione del file audio:", np.size(segnale))

# normalizzo ad 1
data = segnale * 0.9 / np.max(abs(segnale))

winSize = 1024
binsize = fs/winSize

f, t, stft = signal.stft(data, fs, window='blackman', nperseg=winSize,noverlap=winSize//2,nfft=winSize,return_onesided=True, padded=True)
stft = np.transpose(stft)
# calcolo il PSD (Power Spectral Density)
audio_power = np.square(np.abs(stft))
print(audio_power.shape)

# parametri del banco MEL
freq_min = 0
freq_max = fs / 2
mel_filter_num = 20

mel_min = freq_to_mel(freq_min)
mel_max = freq_to_mel(freq_max)
mfbounds = np.linspace(mel_min, mel_max, mel_filter_num)
mfvertex = [(mfbounds[n+1]+mfbounds[n])/2 for n in range(len(mfbounds[:-1]))]

#ffbounds = mel_to_freq(mfbounds)
#ffvertex = mel_to_freq(mfvertex)

def build_triangle(index, filter_num):
    result = np.zeros(filter_num*2)
    result[index+1] = 1
    return (result)

print(mfvertex)
