import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.stats import gmean
def sts (sig, frameSize): #short time signal matrix
    # calcolo il numero di frame arrotondato all'intero più grande
    Nframe = math.ceil(len(sig)/frameSize)

    STS = np.zeros((Nframe,frameSize))

    # copio il segnale in un nuovo vettore
    sigFramed = np.zeros(Nframe*frameSize)

    sigFramed[:len(sig)] = sig

    for i in range(Nframe):
        STS[i,:] = sigFramed[(i*frameSize):((i+1)*frameSize)]

    return STS

def rms(v): # calcolo RMS su vettore
    v = np.square(v)
    v = np.mean(v)
    v = np.sqrt(v)
    return v

def energia_t (v): # CALCOLO DELL'ENERGIA NEL DOMINIO DEL TEMPO
    e_t = np.sum(np.square(v))
    return e_t

def t_centroid(T):
    matIndex = np.arange(np.shape(T)[1]) + 1
    matIndex = np.tile(matIndex, (np.shape(T)[0], 1))
    T = np.abs(T)
    numeratore = np.sum(T*matIndex, axis = 1)
    denominatore = np.sum(T, axis = 1)
    return numeratore / denominatore

def autocorr (v):
    hf = len(v)
    corr = np.zeros(len(v)*3)
    v1 = corr
    v2 = corr
    v1[hf: 2*hf] = v
    #v2[0:len(v)] = v
    for delay in range (len(corr)-len(v)):
        v2 = np.zeros(len(v)*3)
        v2[delay:delay + len(v)] = v
        corr[delay] = np.sum(v1*v2)
    return corr[:2*hf]

def s_centroid(M): # SPECTRAL CENTROID: M is the matrix of STFT

    # costruisco la matrice degli indici
    matIndex = np.arange(np.shape(M)[1])+1
    matIndex = np.tile(matIndex,(np.shape(M)[0],1))
    # calcolo il numeratore del SC
    numeratore = np.sum(abs(M) * matIndex, axis=1)
    # calcolo il denominatore del SC
    denominatore = np.sum(abs(M), axis=1)
    # numeratore / denominatore
    return numeratore / denominatore

def s_flatness(M): # SPECTRAL FLATNESS IN DB: M is the matrix of STFT
    # calcolo la media geometrica
    g = gmean(abs(M), axis = 1)
    # media aritmetica
    a = np.mean(abs(M), axis = 1)
    return 20*np.log10(g/a)

def gen_sine_wave(f0, sampling_frequency, frame_size, phase=0):
    """Generates a sine wave of frequency f0.

    :param f0: float, fundamental frequency
    :param sampling_frequency: int, number of samples per second
    :param frame_size: int, number of samples in frame
    :return:
        - waveform - ndarray of waveform
    """
    t = np.arange(frame_size) / sampling_frequency
    return np.sin(2 * np.pi * f0 * t + phase)

def gen_harmonic_wave(f0, sampling_frequency, frame_size, n_harmonics=10):
    """Generates a 1/f weighted harmonic (multiples of f0) wave of frequency f0.

    :param f0: float, fundamental frequency
    :param sampling_frequency: int, number of samples per second
    :param frame_size: int, number of samples in frame
    :param n_harmonics: int, number of harmonics to add
    :return:
        - waveform - ndarray of waveform
    """
    waveform = np.zeros((frame_size,), dtype=float)
    for f in [f0 * i for i in range(1, n_harmonics + 1)]:
        waveform += f0 / f * gen_sine_wave(f, sampling_frequency, frame_size, phase=f)
    return waveform

def gen_formant_filter(sample_freq, frame_size, filter_frequency = 2000, offset=0.1):
    """Generates a cosin frequency response which can be used to give a signal a formant shaping

    :param sample_freq: sampling frequency
    :param frame_size: int, number of samples in frame
    :param filter_frequency: period in terms of frequency of the formant filter. Default is 2000
    :param offset: additive offset
    :return:- the array of frequencies
            - the array of frequency response in the frequency domain
    """
    dt = 1 / sample_freq
    freq_vector = np.fft.rfftfreq(frame_size, d=dt)
    H = np.zeros(freq_vector.size, dtype=complex)
    for i in range(freq_vector.size):
        H[i] = 2 * (1 - np.cos(2 * np.pi * freq_vector[i] / filter_frequency)) + offset
    return freq_vector, H

def cepstrum(x):
    """ Calculates the Cepstrum of a signal

    :param x: the input signal
    :return: the Cepstrum of the input signal
    """
    return np.abs(np.fft.rfft(np.log(np.abs(np.fft.rfft(x)))))

def freq_to_mel(freq):
    return 2595.0 * np.log10(1.0 + freq / 700.0)

def mel_to_freq(mels):
    return 700.0 * (10.0**(mels / 2595.0) - 1.0)