import numpy as np
import matplotlib.pyplot as plt

N = 64
k0 = 5.65649

X = np.array([])

nv = np.arange(-N/2, N/2)
kv = np.arange(-N/2, N/2)

x = np.cos(2*np.pi*k0*nv/N)

for k in kv:
    s = np.exp(1j*2*np.pi*k*nv/N)
    X = np.append(X, sum(x*np.conjugate(s)))

plt.figure(1)
plt.subplot(311)
plt.plot(nv, x, 'b-x', lw=1.5)
plt.axis([-N/2,N/2,-1,1])

plt.subplot(312)
plt.title('spettro dei moduli')
plt.plot(kv, abs(X), 'r-x', lw=1.5)
plt.axis([-N/2, N/2, 0, N])

plt.subplot(313)
plt.title('spettro delle fasi')
plt.plot(kv, np.unwrap(np.angle(X)), 'c-x', lw=1.5)
#plt.axis([-N/2, N/2, -np.pi, np.pi])

plt.show()