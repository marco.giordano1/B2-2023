from s0_functions import *

import matplotlib.pyplot as plt
from scipy.io.wavfile import read

[fs, segnale] = read("../sounds/speech-female.wav")

# normalizzo il segnale
segnale = segnale / np.max(abs(segnale))

STS = sts(segnale,400)
RMS = [rms(f) for f in STS]
TCEN = t_centroid(STS)
AC = [autocorr(f) for f in STS]

plt.subplot(312)
plt.plot(RMS, 'r')
plt.subplot(311)
plt.plot(segnale, 'b')
plt.subplot(313)
plt.plot(TCEN, 'g')
plt.show()

fig = plt.figure(2)
cmap = plt.pcolormesh(np.transpose(AC), vmin=0, vmax=np.max(AC))
fig.colorbar(cmap)
plt.title('Autocorrelation')
plt.ylabel('delay')
plt.xlabel('frame')
plt.show()

plt.figure(3)
plt.plot(AC[200])
plt.show()